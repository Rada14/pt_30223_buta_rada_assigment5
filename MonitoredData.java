import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class MonitoredData {

    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private String activityLabel;
    List <MonitoredData> md = new ArrayList<MonitoredData>();

    public int getStartDay() {
        return startTime.getDayOfYear();
    }

    public Duration getDuration() {
        return Duration.between(startTime, endTime);

    }

    public void setListOfMonitoredData(List<MonitoredData> l) {
        this.md= l;
    }

    public List<MonitoredData> getListOfMonitoredData() {
        return this.md;
    }



    public MonitoredData(String st, String et, String act) {
        DateTimeFormatter d = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        this.startTime = LocalDateTime.parse(st, d);
        this.endTime = LocalDateTime.parse(et, d);
        this.activityLabel = act;
    }
  

    public MonitoredData(LocalDateTime st, LocalDateTime et, String act ){
      
        this.startTime = st ;
        this.endTime = et;
        this.activityLabel = act;
    }


    


    public void setStartTime(LocalDateTime s) {
        this.startTime = s;
    }

    public LocalDateTime getStartTime() {
        return this.startTime;
    }

    public void setEndTime(LocalDateTime e) {
        this.endTime = e;
    }
    public LocalDateTime getEndTime() {
        return this.endTime;
    }

    public void setActivityLabel(String a) {
        this.activityLabel = a;
    }

    public String getActivityLabel() {
        return this.activityLabel;
    }


    public MonitoredData() {}

    public String toString() {
        return startTime+ "\t\t" + endTime + "\t\t" +  activityLabel;
    }

}
