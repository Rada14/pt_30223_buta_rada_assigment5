import java.io.File;
import java.time.LocalDateTime;
import java.io.FileWriter;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.time.Duration;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.nio.file.Paths;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import static java.util.stream.Collectors.toMap;

public class Tasks {
    private static List<MonitoredData> list; // the list of activities

    /*TASK 1*/
    //reading from Activity.txt file
    public static void readFromFile() {
        list= new ArrayList<MonitoredData>();
        try {

             Stream<String> streamList = Files.lines(Paths.get("Activities.txt"));
                 list= streamList.map(l->l.split("\t\t", -2))
                    .map(res -> new MonitoredData(res[0], res[1], res[2]))
                    .collect(Collectors.toList());

            System.out.println("Activities.txt contains  : \n  ");
            list.forEach(md->System.out.println(md));


        } catch(Exception exc) {
            System.out.println("Read from file  error!!");
            exc.printStackTrace();
        }

    }

    public static void task1() {

        try {
            FileWriter fw = new FileWriter("Task_1.txt");
            for(MonitoredData md: (ArrayList<MonitoredData>)list) {
                fw.write(md + "\n");


            }
            fw.close();

        } catch (Exception exc) {
            exc.getCause();
            System.out.println("TASK 1 CREATING ERROR!");
        }

    }

  /*  public String getDay(String date1) {
        try {
            String [] spl= date1.split(" ");
            String date= spl[0];
            spl = date.split("-");
            String day= spl[2];
            return day;
        }catch(NumberFormatException ex) {
            ex.getCause();
        }

        return null;
    }
*/
  /*  public int getUniqueDay(String date1) {
        try {
            String [] spl = date1.split(" ");
            String date = spl[0];
            spl = date.split("-");
            String month = spl[1];
            String day = spl[2];
            return Integer.parseInt(month)*31+Integer.parseInt(day);
        }catch(NumberFormatException exc) {
            exc.getCause();
        }

        return 0;
    }
*/

    /* TASK 2  one version without stream */

  /*  public long countDistinctDays() {
        List<String> startTime = new ArrayList<String>();
        List<String> endTime = new ArrayList<String>();
        List<Integer> sumList = new ArrayList<Integer>();

        LocalDateTime ldt1= null;
        LocalDateTime ldt2=null;



        for(MonitoredData md: list) {
            ldt1 = md.getStartTime();
            ldt2=md.getEndTime();
            String s1 = ldt1.toString();
            String s2=ldt2.toString();
            startTime.add(s1);
            endTime.add(s2);

        }

        for(String s: startTime){
            int x = getUniqueDay(s);
            sumList.add(x);
        }

        for(String s: endTime){
            int y=getUniqueDay(s);
            sumList.add(y);
        }

        long countList = sumList.stream().distinct().count();


        System.out.println("Distinct days : "+ countList);

        return countList;
    } */



    /*TASK 2*/
  // sum of the distinct days
   public static long calculateDistinctDays() {
        long calc = list.stream()
                .map(activity->activity.getStartDay()).distinct().count();
        return calc;
    }

    public void task2() {

       System.out.println("\n  TASK 2: ");

        try {
            System.out.println("There are " + calculateDistinctDays() + " distinct days .");
            FileWriter fw = new FileWriter("Task_2.txt");
            fw.write("In the list are  " + calculateDistinctDays()+" distinct days  .\n");
            fw.close();

        } catch(Exception e) {
            e.getCause();
            System.out.println("Error creating  Task_2.txt! ");
        }
    }


    /*TASK 3*/
    //number of each activity
    public static Map<String, Integer> countActivities(){
        Map<String,Integer> activities = list.stream()
                .collect(Collectors.groupingBy(MonitoredData::getActivityLabel, Collectors.summingInt(e->1)));
        return activities;
    }

    public static void task3() {

        System.out.println("\n TASK 3: ");
        try {
            FileWriter fw= new FileWriter("Task_3.txt");

            for(Map.Entry<String, Integer> entry: countActivities().entrySet()) {
                fw.write("Activity " + entry.getKey()+ " has appeared "+entry.getValue()+" times. \n");

            }
            fw.close();

        } catch (Exception e) {
            e.getCause();
            System.out.println("Task 3 error!");
        }

    }

    /*TASK 4*/
    //number of each day's activities
    public static Map<Integer, Map<String, Integer>> countDailyActivities(){
        Map<Integer, Map<String,Integer>>  dailyActivities = list.stream()
                .collect(Collectors.groupingBy(MonitoredData::getStartDay,
                        Collectors.groupingBy(MonitoredData::getActivityLabel, Collectors.summingInt(e->1))));
        return dailyActivities;
    }

    public static void task4() {

        System.out.println("\n TASK 4: ");
        try {
            FileWriter fw = new FileWriter("Task_4.txt");

            for(Map.Entry<Integer, Map<String, Integer>> entry : countDailyActivities().entrySet()) {
                fw.write("Day :" + entry.getKey()+ " has appeared "+entry.getValue()+"\n");
                System.out.println(entry.getKey()+ " has appeared "+entry.getValue());
            }
            fw.close();

        } catch (Exception e) {
            e.getCause();
            System.out.println("Error creating  task_4.txt !");
        }

    }

    /*TASK 5*/
    // duration of activities
    public static Map<String, Duration> calculateDuration(){
        Map<String, Duration> duration = list.stream()
                .collect(toMap(MonitoredData::getActivityLabel, MonitoredData::getDuration, Duration::plus));
        for (MonitoredData md: list) {
            duration.merge(md.getActivityLabel(), md.getDuration(), Duration::plus);
        }

        return duration;
    }

    public static void task5() {

        System.out.println("\n TASK 5: ");
        try {
            FileWriter fw = new FileWriter("Task_5.txt");
            for(Map.Entry<String, Duration> entry : calculateDuration().entrySet()) {
                System.out.println(entry.getKey()+ " has a duration of "+entry.getValue().toHours()/2+" hours and "+entry.getValue().toMinutes()/2+" minutes");
                fw.write("Activity " + entry.getKey()+ " has a duration of "+entry.getValue().toHours()/2+" hours and "+entry.getValue().toMinutes()/2+" minutes \n");

            }
            fw.close();

        } catch (Exception exc) {
            exc.getCause();
            System.out.println("Error creating task_5.txt !");
        }

    }

    public Tasks() {
        readFromFile();
        task1();
        task2();
        task3();
        task4();
        task5();
    }



}
